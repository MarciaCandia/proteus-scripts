#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import csv
#from datetime import datetime
import sys
from proteus import Model
from proteus import config as pconfig
from optparse import OptionParser
from decimal import Decimal
from trytond.transaction import Transaction

import random


def main(options):
    config = pconfig.set_trytond(options.database, options.admin, config_file=options.config_file)
    Lang = Model.get('ir.lang')
    (es,) = Lang.find([('code', '=', 'es')])
    es.translatable = True
    es.save()

    create_patients(config, es, options)
    print ("done.")

def create_patients(config, lang, options):
    ProductTemplate = Model.get('product.template')
    Product = Model.get('product.product')
    Medicament = Model.get('gnuhealth.medicament')
    
    UOM = Model.get('product.uom')
    CATEGORY = Model.get('product.category')
    context = {'language':'en'}

    uom = UOM.find([('name','=','Unidad')])
    category = CATEGORY.find([('childs', '=', 'REMEDIAR')])
    
    with open(options.file_csv,encoding="utf-8") as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=",")

        next(csv_reader) #skip header
        
        
        for line in csv_reader:
            '''Put all row data into variables'''
            code = line[0]
            name = line[1]
            is_traceable = False if line[2] == '0' else True
            GTIN = line[3]
            type_ = 'goods'
            is_medicament = True if line[7] == '1' else False
            list_price = line[4]
            
            '''search if already exists'''
            template = ProductTemplate.find(['name','=',name])
            if len(template) == 0:
                
                template = ProductTemplate(
                        name = name,
                        type = type_,
                        default_uom = uom[0].id,
                        list_price = Decimal(list_price),
                        #category = category[0].id                   
                        )
                template.save()
            else:
                template = template[0]
                
            product = Product.find([
                            ('anmat_gtin_indicator_digits','=','01'),    
                            ('anmat_gtin','=',GTIN)
                            ])
            if len(product) == 0:
                product = Product(
                    template = template.id,
                    code = code or '',
                    anmat_traceable = is_traceable,
                    anmat_gtin = '0' + GTIN,
                    anmat_gtin_indicator_digits = '01',
                    is_medicament = is_medicament
                    )
                product.save()
            else:
                product = product[0]                
            
            medicament = Medicament.find([('name','=',product.id)])
            if len(medicament) == 0 and product.is_medicament:
                medicament = Medicament(
                    name = product.id,
                    )
                medicament.save()


if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-d', '--database', dest='database')
    parser.add_option('-c', '--config-file', dest='config_file')
    parser.add_option('-a', '--admin', dest='admin')
    parser.add_option('-f','--file',dest='file_csv')
    parser.set_defaults(user='admin')

    options, args = parser.parse_args()
    if args:
        parser.error('Parametros incorrectos')
    if not options.database:
        parser.error('Se debe definir [nombre] de base de datos')
    if not options.config_file:
        parser.error('Se debe definir el path absoluto al archivo de '
            'configuracion de trytond')
    if not options.admin:
        parser.error('Se debe definir el usuario admin')
    if not options.file_csv:
        parser.error('Se debe especificar el archivo csv')
    
    main(options)
   
